/**
  ******************************************************************************
  * File Name          : USART.h
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __usart_H
#define __usart_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;

/* USER CODE BEGIN Private defines */
typedef struct {
	uint16_t rx_buff_overrun : 			1;
	uint16_t packet_rec : 				1;
	uint16_t in_rx_buff : 				1;
	uint16_t in_tx_buff : 				1;
	uint16_t tx_busy : 					1;
} ux_flag_t;

#define UXRX_DMA_BUFF_SIZE				512
#define UXTX_DMA_BUFF_SIZE				256
#define UXRX_BUFF_SIZE					512
#define UXTX_BUFF_SIZE					256

typedef struct UartRx_Socket URxSKT;
struct UartRx_Socket {		// total size must be dividable by 4
	ux_flag_t UxFlag;
	uint16_t  RxHead;
	uint16_t  RxTail;
	uint16_t  RxCnts;
	uint8_t   RxBuff[UXRX_BUFF_SIZE];
	uint8_t   RxDMABuff[UXRX_DMA_BUFF_SIZE];
};
extern URxSKT U2RxSKT;
extern URxSKT U3RxSKT;

/* USER CODE END Private defines */

void MX_USART2_UART_Init(void);
void MX_USART3_UART_Init(void);

/* USER CODE BEGIN Prototypes */
uint16_t UART_GetRxBuffSize( UART_HandleTypeDef* huart );
void UART_Gets( UART_HandleTypeDef* huart, uint8_t* outbuf, uint16_t len );
void UART_Puts( UART_HandleTypeDef* huart, uint8_t* inbuff, uint16_t len );
void UART3_Puts( uint8_t* inbuff, uint16_t len );

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ usart_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
