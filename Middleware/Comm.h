#ifndef __COMM_H
#define __COMM_H

#define dim(x) 				(sizeof(x) / sizeof(x[0]))   /* Returns number of elements	*/
#define ARRAY_SIZE(x)    	(sizeof(x) / sizeof((x)[0]))

typedef void(*PFNCT_PUT)(uint8_t*, uint16_t);


typedef struct {
	struct list_head list;
	uint16_t length;
	uint8_t* dataPtr;
} CmdNode_t;


#endif /* #ifndef __COMM_H */
