/*******************************************************************************

(C) Copyright 2015, Chaoyi Dai. 	 All rights reserved

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
Version: V0.01
Author: Chaoyi Dai
Last Modify Date: 2015/10/25
Discription: Initial version

*******************************************************************************/

#include "includes.h"
#include "stm32f1xx_hal.h"
#include "usart.h"

#if 0
LIST_HEAD( CmdList_Head );

static enum {
	COMM_INIT = 0,
	COMM_GET_CMD,
	COMM_SEND_CMD,
	COMM_WAIT_RESP,
} CommSM = COMM_INIT;

static bool isStart = false;
static uint32_t timeout;
static uint32_t timeRequest;
static uint8_t retry = 1;
static CmdNode_t* cmdNode;

void Cmd_EnQueue( struct list_head* head, uint8_t* data, uint32_t len )
{
	uint8_t *dataPtr = NULL;
	CmdNode_t* cmdNode;
	
	dataPtr = malloc(len);
	if( dataPtr == NULL ) {
		return;
	}
	memcpy( dataPtr, data, len );
	
	cmdNode = malloc(sizeof(CmdNode_t));
	if( cmdNode == NULL ) {
		free(dataPtr);
		return;
	}
	cmdNode->length  = len;
	cmdNode->dataPtr = dataPtr;
	INIT_LIST_HEAD(&cmdNode->list);
	
	__disable_interrupt();
	list_add_tail(&cmdNode->list, head);
	__enable_interrupt();
}

CmdNode_t* Cmd_DeQueue( struct list_head* head )
{
	CmdNode_t* cmdNode;
	
	__disable_interrupt();
	cmdNode = list_first_entry_or_null(head, CmdNode_t, list);
	if( cmdNode != NULL ) {
		list_del(&cmdNode->list);
	}
	__enable_interrupt();

	return( cmdNode );
}

void Cmd_Delete( CmdNode_t* cmdNode )
{
	if( cmdNode != NULL ) {
		if( cmdNode->dataPtr != NULL ) {
			free(cmdNode->dataPtr);
		}
		free(cmdNode);
	}
}

bool IsCmdQueueEmpty( const struct list_head* head )
{
	return( list_empty(head) );
}

void Comm_Host2Send( void )
{
	switch( CommSM ) {
		case COMM_INIT:
			if( IsCmdQueueEmpty() == true ) {
				break;
			}
			
			CommSM = COMM_GET_CMD;
			break;
		case COMM_GET_CMD:
			if( cmdNode != NULL ) {
				Cmd_Delete(cmdNode);
			}
			
			cmdNode = Cmd_DeQueue();
			if( cmdNode != NULL ) {
                retry = 1;
                timeout = TICK_SECOND / 5;		//200ms delay
				CommSM = COMM_SEND_CMD;
			}
			else {
				isBusy = false;
				CommSM = COMM_INIT;
			}
			break;
		case COMM_SEND_CMD:
			wasResponseGot = false;
			isBusy = true;
            
            DRV_UART1_InitializerDefault(); // make sure the UART buffer clean
			
                ClearRxData();
                Uart1_RS485_Send(cmdNode->target, cmdNode->dataPtr, cmdNode->length);
			timeRequest = TickGet();
            CommSM = COMM_WAIT_RESP;
			break;
        case COMM_WAIT_RESP:
            if( TickGet() - timeRequest >= timeout ) {
                if (retry == 0) {
                    
                    isBusy = false;
                    CommSM = COMM_INIT;
                }
				else {
                    retry--;
                    CommSM = COMM_SEND_COMMAND;
                }
            }
            else {
                if (wasResponseGot == true) {
                    isBusy = false;
                    CommSM = COMM_INIT;
                }
            }
			break;
		default:
			CommSM = COMM_INIT;
			break;
	}
}

#endif
