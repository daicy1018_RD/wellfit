/*******************************************************************************

(C) Copyright 2015, Chaoyi Dai. 	 All rights reserved

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
Version: V0.01
Author: Chaoyi Dai
Last Modify Date: 2015/10/25
Discription: Initial version

*******************************************************************************/

#include "includes.h"
#include "stm32f1xx_hal.h"
#include "usart.h"

__no_init hmi_t InstanceHmi;


void hmi_Init( hmi_t* instance )
{
	if( instance != NULL ) {
		instance->hmi_ctrl.parser_success = false;
		instance->offset = 0;
		instance->stateStep = HMI_S_SOP1;
	}
}

void hmi_CommInit( void )
{
    hmi_t* hmi = &InstanceHmi;
    hmi_Init( hmi );
	hmi->hmi_ctrl.crc_en = true;
    hmi->pf_output = (PFNCT_PUT)UART3_Puts;
}

bool hmi_ParsingByte( hmi_t* instance, uint8_t inbyte )
{
	uint16_t crc = 0x0;
	
	switch( instance->stateStep ) {
		case HMI_S_SOP1:
			if( inbyte == HMI_COMM_SOP1 ) {
				instance->stateStep++;
			}
			break;
		case HMI_S_SOP2:
			if( inbyte == HMI_COMM_SOP2 ) {
				instance->hmi_ctrl.was_resp_got   = false;
				instance->hmi_ctrl.parser_success = false;
				instance->stateStep++;
			}
			else {
				instance->stateStep = HMI_S_SOP1;
			}
			break;
		case HMI_S_LEN:
			instance->rxdatalen  = inbyte;
			instance->rxdatasize = inbyte;
			instance->offset	 = 0;
			instance->stateStep++;
			break;
		case HMI_S_CMD:
			instance->command = inbyte;
			instance->rxdata[instance->offset++] = inbyte;
			instance->rxdatasize--;
			instance->stateStep++;
			break;
		case HMI_S_DATA:
			instance->rxdata[instance->offset++] = inbyte;
			if( --instance->rxdatasize == 0 ) {
				if( instance->hmi_ctrl.crc_en == true ) {
					instance->stateStep++;
				}
				else {
					instance->hmi_ctrl.parser_success = true;
					instance->stateStep = HMI_S_SOP1;
					return( true );
				}
			}
			break;
		case HMI_S_CRC1:
			instance->crc = (uint16_t)(inbyte << 8);
			instance->stateStep++;
			break;
		case HMI_S_CRC2:
			instance->crc |= inbyte;
			crc = Crc16(instance->rxdata, instance->rxdatalen);
			if( crc == instance->crc ) {
				instance->hmi_ctrl.parser_success = true;
			}
			else {
				Utl_MemSet( (uint8_t*)instance, 0x00, sizeof(hmi_t) );
			}
			instance->stateStep = HMI_S_SOP1;
			return( true );
		default:
			Utl_MemSet( (uint8_t*)instance, 0x00, sizeof(hmi_t) );
			instance->stateStep = HMI_S_SOP1;
			break;
	}
	return( false );
}

void hmi_BuildPkt( hmi_t* instance )
{
	uint16_t i = 0;
	uint16_t crc  = 0x0;
	uint16_t len  = instance->txdatalen;
	uint8_t* data = instance->txdata;
	uint8_t* buf  = instance->txpkt;
	
	Utl_MemSet( buf, 0x0, HMI_PKG_SIZE );
	buf[i++] = HMI_COMM_SOP1;
	buf[i++] = HMI_COMM_SOP2;
	buf[i++] = len;
	buf[i++] = instance->command;
	if( len > 0 ) {
		Utl_MemCopy( &(buf[i]), data, len );
		i += len;
	}
	crc = Crc16(&(buf[3]), (len + 1));
	buf[i++] = (uint8_t)(crc >> 8);
	buf[i++] = (uint8_t)(crc >> 0);
    if( i > HMI_PKG_SIZE ) {
        i = HMI_PKG_SIZE;
    }
    if( instance->pf_output != (PFNCT_PUT)NULL ) {
        instance->pf_output( buf, i );
    }
}

/* return 1: support, 0: not support */
uint8_t hmi_SearchCmd( uint8_t cmd, PFNCT_HMI* func, PFNCT_HMI* respfunc )
{
	uint16_t i;
	HmiCmdNode* tptr;
	
	/* Search command set */
	tptr = (HmiCmdNode*)HmiCmdTable;
	for( i=0; i<HmiCmdTotal; i++ ) {
		if( cmd == (tptr+i)->CmdCode ) {
			*func 	  = (PFNCT_HMI)(tptr+i)->pf_func;
			*respfunc = (PFNCT_HMI)(tptr+i)->pf_respfunc;
			return( true );
		}
	}
	return( false );
}

void hmi_Host2Hmi( hmi_t* instance, uint8_t cmd )
{
	PFNCT_HMI hostfunc = NULL;
	PFNCT_HMI respfunc = NULL;
	
	instance->command = cmd;
	if( hmi_SearchCmd(instance->command, &hostfunc, &respfunc) == 0 ) {
		return;
	}
	instance->txdatalen = 0;
	if( hostfunc != NULL ) {
		hostfunc( instance );
	}
	hmi_BuildPkt( instance );
}

void hmi_RcvAction( hmi_t* instance )
{
	PFNCT_HMI func  = NULL;
	PFNCT_HMI respf = NULL;
	
	if( hmi_SearchCmd(instance->command, &func, &respf) == 0 ) {
		return;
	}
	
	if( respf != NULL ) {
		respf( instance );
	}
}

void hmi_HostRespAction( hmi_t* instance )
{
	PFNCT_HMI func  = NULL;
	PFNCT_HMI respf = NULL;
	
	if( hmi_SearchCmd(instance->command, &func, &respf) == 0 ) {
		return;
	}
	instance->txdatalen = 0;
	if( respf != NULL ) {
		respf( instance );
	}
	//hmi_BuildPkt( instance );
}

void hmi_WrReg( uint16_t addr, uint8_t* data, uint16_t len )
{
	uint16_t i = 0;
	hmi_t* instance = &InstanceHmi;
	
	instance->txdata[i++] = addr;
	memcpy( &instance->txdata[i], data, len );
	i += len;
	instance->txdatalen = i;
	instance->command = HMI_WR_REG;
	hmi_BuildPkt( instance );
}

void hmi_RdReg( uint16_t addr, uint16_t len )
{
	uint16_t i = 0;
	hmi_t* instance = &InstanceHmi;
	
	instance->txdata[i++] = addr;
	instance->txdata[i++] = len;
	instance->txdatalen = i;
	instance->command = HMI_RD_REG;
	hmi_BuildPkt( instance );
}

void hmi_WrMem( uint16_t addr, uint8_t* data, uint16_t len )
{
	uint16_t i = 0;
	hmi_t* instance = &InstanceHmi;
	
	instance->txdata[i++] = addr;
	memcpy( &instance->txdata[i], data, len );
	i += len;
	instance->txdatalen = i;
	instance->command = HMI_WR_MEM;
	hmi_BuildPkt( instance );
}

void hmi_RdMem( uint16_t addr, uint16_t len )
{
	uint16_t i = 0;
	hmi_t* instance = &InstanceHmi;
	
	instance->txdata[i++] = addr;
	instance->txdata[i++] = len;
	instance->txdatalen = i;
	instance->command = HMI_RD_MEM;
	hmi_BuildPkt( instance );
}

/* Command set table */
const HmiCmdNode HmiCmdTable[] = {
	//HMI_WR_REG,		hmi_WrReg,				NULL,
	//HMI_RD_REG,		hmi_RdReg,				hmi_RdRegResp,
	//HMI_WR_MEM,		hmi_WrMem,				NULL,
	HMI_RD_MEM,		NULL,				NULL,
};
const uint16_t HmiCmdTotal = dim(HmiCmdTable);

void Comm_WithHMITask( void )
{
	uint8_t  inbyte;
	uint16_t rxlen;
	hmi_t*   instance = &InstanceHmi;
	
	rxlen = UART_GetRxBuffSize( &huart3 );
	while( rxlen-- ) {
		UART_Gets( &huart3, &inbyte, 1 );
		if( hmi_ParsingByte(instance, inbyte) == true ) {
			hmi_RcvAction( instance );
			instance->hmi_ctrl.was_resp_got = true;
			instance->hmi_ctrl.comm_excep   = false;
		}
	}
}

