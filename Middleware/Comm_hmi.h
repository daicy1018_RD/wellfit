#ifndef __COMM_HMI_H
#define __COMM_HMI_H

//commands definition list
#define HMI_WR_REG          0x80
#define HMI_RD_REG          0x81
#define HMI_WR_MEM          0x82
#define HMI_RD_MEM          0x83

#define HMI_COMM_SOP1		0x5A
#define HMI_COMM_SOP2		0xA5

typedef enum {
	HMI_S_SOP1 = 0,
	HMI_S_SOP2,
	HMI_S_LEN,
	HMI_S_CMD,
	HMI_S_DATA,
	HMI_S_CRC1,
	HMI_S_CRC2,
} hmi_state_t;

typedef struct {
	uint16_t crc_en : 			1;
	uint16_t was_resp_got : 	1;
    uint16_t parser_success :   1;
	uint16_t comm_excep :   	1;
	uint16_t need2_resp :		1;
} hmi_control_t;

#define HMI_RX_DATA_SIZE		256
#define HMI_TX_DATA_SIZE		200
#define HMI_PKG_SIZE			256

typedef struct {
	hmi_control_t hmi_ctrl;
	hmi_state_t   stateStep;
	
	uint16_t	command;
	uint16_t	offset;
	uint16_t	rxdatasize;
	uint16_t	rxdatalen;
	uint16_t	txdatalen;
	uint8_t		rxdata[HMI_RX_DATA_SIZE];
	uint8_t		txdata[HMI_TX_DATA_SIZE];
	uint8_t		txpkt[HMI_PKG_SIZE];
	PFNCT_PUT	pf_output;
	uint8_t		retry;
	uint8_t:	8;
	uint16_t	timeout;
	uint16_t	crc;
}hmi_t;
extern __no_init hmi_t InstanceHmi;

typedef void(*PFNCT_HMI)(hmi_t*);
typedef struct {
	uint8_t CmdCode;
	PFNCT_HMI pf_func;
	PFNCT_HMI pf_respfunc;
}HmiCmdNode;
extern const uint16_t HmiCmdTotal;
extern const HmiCmdNode HmiCmdTable[];

void hmi_Init( hmi_t* instance );
void hmi_CommInit( void );
bool hmi_ParsingByte( hmi_t* instance, uint8_t inbyte );
void hmi_BuildPkt( hmi_t* instance );
uint8_t hmi_SearchCmd( uint8_t cmd, PFNCT_HMI* func, PFNCT_HMI* respfunc );
void hmi_RcvAction( hmi_t* instance );
void hmi_HostRespAction( hmi_t* instance );
void hmi_Host2Hmi( hmi_t* instance, uint8_t cmd );

void Comm_WithHMITask( void );

#endif /* #ifndef __COMM_HMI_H */
