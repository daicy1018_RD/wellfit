/*******************************************************************************

(C) Copyright 2015, Chaoyi Dai. 	 All rights reserved

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
Version: V0.01
Author: Chaoyi Dai
Last Modify Date: 2015/10/25
Discription: Initial version

*******************************************************************************/

#include "includes.h"
#include "stm32f1xx_hal.h"
#include "usart.h"


const acs_patten_t acs_pattens[] = {
	{.ver=0x01, .sopSize=1, .verSize=1, .cmdSize=1, .dataLenSize=1, .datacrcSize=1},
};

__no_init acs_t InstanceDashboard;


void acs_Init( acs_t* instance )
{
	if( instance != NULL ) {
		memset( &(instance->header), 0x0, sizeof(acs_header_t) );
		instance->acs_ctrl.parser_success = false;
		instance->pos  = 0;
		instance->size = 0;
		instance->stateStep = ACS_SOP;
	}
}

void acs_WithDashboardInit( void )
{
    acs_t* acs = &InstanceDashboard;
    acs_Init( acs );
    acs->pf_output = (PFNCT_PUT)UART3_Puts;
}

acs_patten_t* acs_GetVerPatten( uint8_t ver )
{
	acs_patten_t* patten;
	uint8_t i;
    
	for( i=0; i<ARRAY_SIZE(acs_pattens); i++ ) {
		patten = (acs_patten_t *)&acs_pattens[i];
		if (ver == patten->ver) {
			return( patten );
		}
	}
	
	return( NULL );
}

bool acs_ParsingByte( acs_t* instance, uint8_t inbyte )
{
	uint8_t* header = (uint8_t*)&(instance->header);
	uint8_t  crc;
	
	switch( instance->stateStep ) {
		case ACS_SOP:
			instance->pos = 0;
			if( inbyte == COMM_SOP ) {
				instance->acs_ctrl.is_busy = true;
				header[instance->pos++] = inbyte;
				instance->stateStep++;
			}
			break;
		case ACS_VER:
			header[instance->pos++] = inbyte;
			instance->stateStep++;
			break;
		case ACS_CMD_ID:
			header[instance->pos++] = inbyte;
			instance->stateStep++;
			break;
		case ACS_DATA_LEN:
			header[instance->pos++] = inbyte;
			if( instance->header.datalen == 0 ) {
				instance->stateStep = ACS_SOP;
				return( true );
			}
			else {
				instance->stateStep++;
			}
			break;
		case ACS_DATA_CRC:
			header[instance->pos++] = inbyte;
			instance->size = instance->header.datalen;
			instance->pos  = 0;
			instance->stateStep++;
			break;
		case ACS_DATA:
			instance->rxdata[instance->pos++] = inbyte;
			if( --instance->size == 0 ) {
				crc = crc8(instance->rxdata, instance->header.datalen);
				if( crc == instance->header.datacrc ) {
					instance->stateStep = ACS_SOP;
					return( true );
				}
				else {
					Utl_MemSet( (uint8_t*)instance, 0, sizeof(acs_t) );
					instance->stateStep = ACS_SOP;
				}
			}
			break;
		default:
			Utl_MemSet( (uint8_t*)instance, 0, sizeof(acs_t) );
			instance->stateStep = ACS_SOP;
			break;
	}
	return( false );
}

void acs_BuildPkt( acs_t* instance )
{
	uint16_t i = 0;
	uint16_t len  = instance->txdatalen;
	uint8_t* data = instance->txdata;
	uint8_t* buf  = instance->txpkt;
	
	Utl_MemSet( buf, 0x0, ACS_PKG_SIZE );
	buf[i++] = COMM_SOP;
	buf[i++] = PROTO_VER_0x01;
	buf[i++] = instance->header.command;
	buf[i++] = len;
	if( len > 0 ) {
		buf[i++] = crc8(data, len);
		Utl_MemCopy( &(buf[i]), data, len );
		i += len;
	}
    if( i > ACS_PKG_SIZE ) {
        i = ACS_PKG_SIZE;
    }
    if( instance->pf_output != (PFNCT_PUT)NULL ) {
        instance->pf_output( buf, i );
    }
}

/* return 1: support, 0: not support */
uint8_t acs_SearchCmd( uint8_t cmd, PFNCT_ACS* func, PFNCT_ACS* respfunc )
{
	uint16_t i;
	CmdNode* tptr;
	
	/* Search command set */
	tptr = (CmdNode*)CmdTable;
	for( i=0; i<CmdTotal; i++ ) {
		if( cmd == (tptr+i)->CmdCode ) {
			*func = (PFNCT_ACS)(tptr+i)->pf_func;
			*respfunc = (PFNCT_ACS)(tptr+i)->pf_respfunc;
			return( true );
		}
	}
	return( false );
}

void acs_SalveRcvAction( acs_t* instance )
{
	bool dashboardrsp = false;
	PFNCT_ACS func  = NULL;
	PFNCT_ACS respf = NULL;
	
	if( acs_SearchCmd(instance->header.command, &func, &respf) == 0 ) {
		return;
	}
	switch( instance->header.command ) {
		case ACS_BATT_CAPACITY:
		case ACS_ADJUST_DONE:
		case ACS_ERROR_CODE:
			dashboardrsp = true;
			break;
		default:
			dashboardrsp = false;
			break;
	}
	
	if( dashboardrsp == false ) {
		if( func != NULL ) {
			func( instance );
		}
	}
	
	instance->txdatalen = 0;
	if( respf != NULL ) {
		respf( instance );
	}
	if( dashboardrsp == false ) {
		acs_BuildPkt( instance );
	}
}

void acs_HostRespAction( acs_t* instance )
{
	PFNCT_ACS func  = NULL;
	PFNCT_ACS respf = NULL;
	
	if( acs_SearchCmd(instance->header.command, &func, &respf) == 0 ) {
		return;
	}
	instance->txdatalen = 0;
	if( respf != NULL ) {
		respf( instance );
	}
	//acs_BuildPkt( instance );
}

void acs_Host2Dashboard( acs_t* instance, uint8_t cmd )
{
	PFNCT_ACS hostfunc = NULL;
	PFNCT_ACS respfunc = NULL;
	
	if( instance->acs_ctrl.is_busy == true ) {
		return;
	}
	instance->header.command = cmd;
	if( acs_SearchCmd(instance->header.command, &hostfunc, &respfunc) == 0 ) {
		return;
	}
	instance->txdatalen = 0;
	if( hostfunc != NULL ) {
		hostfunc( instance );
	}
	acs_BuildPkt( instance );
}

static void acs_SysOnOff( acs_t* instance )
{
	//SysMgmt.Status.power_on = (instance->rxdata[0] == 0x01) ? (false) : (true);
}

/* Command set table */
const CmdNode CmdTable[] = {
	ACS_SYS_ON_OFF,			acs_SysOnOff,				NULL,
	
};
const uint16_t CmdTotal = dim(CmdTable);

void Comm_WithLCMTask( void )
{
	uint8_t  inbyte;
	uint16_t rxlen;
	acs_t*   instance = &InstanceDashboard;
	
	rxlen = UART_GetRxBuffSize( &huart3 );
	while( rxlen-- ) {
		UART_Gets( &huart3, &inbyte, 1 );
		if( acs_ParsingByte(instance, inbyte) == true ) {
			acs_SalveRcvAction( instance );
			instance->acs_ctrl.was_resp_got = true;
			instance->acs_ctrl.comm_excep   = false;
			instance->acs_ctrl.is_busy 		= false;
		}
	}
}

void Comm_WithStudioTask( void )
{
	
}
