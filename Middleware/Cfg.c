/*******************************************************************************

(C) Copyright 2014, Chaoyi Dai. 	 All rights reserved

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
Version: V1.00
Author: Chaoyi Dai
Last Modify Date: 5/25/2014
Discription: Initial version

*******************************************************************************/


#include "includes.h"


const SYS_CFG st_CfgDef = {
//************************ System Info data default **********************
	.u16_InitFlg 	= INIT_FLAG,
	0x00000000,
	
};


__no_init SYS_CFG st_Cfg;
bool Cfg_init( void )
{
   	Utl_MemSet( (uint8_t*)&st_Cfg, 0x00, sizeof(SYS_CFG) );
   	Cfg_Rd( (uint16_t*)INFO_BASEADDR, (uint16_t*)&st_Cfg, sizeof(SYS_CFG)/2 );
	if( st_Cfg.u16_InitFlg == INIT_FLAG ) {
		return( true );
	}
	
   	st_Cfg = st_CfgDef;
	Cfg_Wr( (uint16_t*)INFO_BASEADDR, (uint16_t*)&st_Cfg, sizeof(SYS_CFG)/2 );
	return( false );
}

void Cfg_BlkErase( uint16_t* addr )
{
	HAL_FLASH_Unlock();
	FLASH_PageErase( (uint32_t)addr );
	HAL_FLASH_Lock();
}

void Cfg_Wr( uint16_t* addr, uint16_t* buff, uint16_t len )
{
	HAL_FLASH_Unlock();
	while( len-- ) {
		HAL_FLASH_Program( FLASH_TYPEPROGRAM_HALFWORD, (uint32_t)addr++, *buff++ );
	}
	HAL_FLASH_Lock();
}

void Cfg_Rd( uint16_t* addr, uint16_t* buff, uint16_t len )
{
	while( len-- ) {
		*buff++ = *addr++;
	}
}

