/*******************************************************************************

   (C) Copyright 2009/06/27, Chaoyi Dai. 	 All rights reserved

----------------- File Information ---------------------------------------------
       File Name: TM_Utils.c
          Author: Chaoyi Dai
Last Modify Date: 2012/04/17
     Discription: 

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
         Version: V1.00
          Author: Chaoyi Dai
Last Modify Date: 2012/04/17
     Discription: Initial version

*******************************************************************************/

#include "Include.h"


void TM_Dly10us( INT16U u16_us )
{
	while( u16_us ) {
      Delay5US();
      Delay5US();
		u16_us--;
	}
}

void TM_Dlyms( INT16U u16_ms )
{
	while( u16_ms ) {
		TM_Dly10us( 99 );
		u16_ms--;
	}
}

/* EOF */
