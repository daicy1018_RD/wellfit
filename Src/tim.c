/**
  ******************************************************************************
  * File Name          : TIM.c
  * Description        : This file provides code for the configuration
  *                      of the TIM instances.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "tim.h"

#include "gpio.h"

/* USER CODE BEGIN 0 */

#include "includes.h"

TIM_OC_InitTypeDef sCfgOC;
/* USER CODE END 0 */

TIM_HandleTypeDef htim1;

/* TIM1 init function */
void MX_TIM1_Init(void)
{
  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 4800;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  HAL_TIM_Base_Init(&htim1);

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig);

  HAL_TIM_PWM_Init(&htim1);

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig);

  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_LOW;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig);

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1);

  HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2);

  HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3);

  HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4);

}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim_base)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(htim_base->Instance==TIM1)
  {
  /* USER CODE BEGIN TIM1_MspInit 0 */

  /* USER CODE END TIM1_MspInit 0 */
    /* Peripheral clock enable */
    __TIM1_CLK_ENABLE();
  
    /**TIM1 GPIO Configuration    
    PA8     ------> TIM1_CH1
    PA9     ------> TIM1_CH2
    PA10     ------> TIM1_CH3
    PA11     ------> TIM1_CH4 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_11;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* USER CODE BEGIN TIM1_MspInit 1 */

  /* USER CODE END TIM1_MspInit 1 */
  }
}

void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* htim_base)
{

  if(htim_base->Instance==TIM1)
  {
  /* USER CODE BEGIN TIM1_MspDeInit 0 */

  /* USER CODE END TIM1_MspDeInit 0 */
    /* Peripheral clock disable */
    __TIM1_CLK_DISABLE();
  
    /**TIM1 GPIO Configuration    
    PA8     ------> TIM1_CH1
    PA9     ------> TIM1_CH2
    PA10     ------> TIM1_CH3
    PA11     ------> TIM1_CH4 
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11);

  }
  /* USER CODE BEGIN TIM1_MspDeInit 1 */

  /* USER CODE END TIM1_MspDeInit 1 */
} 

/* USER CODE BEGIN 1 */
static const uint32_t Tim_ChannelMap[] = { TIM_CHANNEL_2, TIM_CHANNEL_1, TIM_CHANNEL_4,TIM_CHANNEL_3, TIM_CHANNEL_ALL };
void HAL_TIM_ConfigOC( void )
{
    TIM_OC_InitTypeDef* scfgoc = &sCfgOC;
    
    scfgoc->OCMode = TIM_OCMODE_PWM1;
    scfgoc->Pulse = 0;
    scfgoc->OCPolarity = TIM_OCPOLARITY_HIGH;
    scfgoc->OCNPolarity = TIM_OCNPOLARITY_HIGH;
    scfgoc->OCFastMode = TIM_OCFAST_DISABLE;
    scfgoc->OCIdleState = TIM_OCIDLESTATE_RESET;
    scfgoc->OCNIdleState = TIM_OCNIDLESTATE_RESET;
}

void HAL_TIM_PWM_Output( uint8_t channel, uint32_t duty )
{
    TIM_OC_InitTypeDef* scfgoc = &sCfgOC;
    uint32_t tim_ch;
    
    channel %= PWM_CH_ALL;
    tim_ch = Tim_ChannelMap[channel];
    scfgoc->Pulse = duty;
    HAL_TIM_PWM_ConfigChannel(&htim1, scfgoc, tim_ch);
	if( duty > 0 ) {
		HAL_TIM_OC_Start( &htim1, tim_ch );
	}
	else {
		HAL_TIM_OC_Stop( &htim1, tim_ch );
	}
}

void HAL_TIM_PWM_Reset( void )
{
	HAL_TIM_PWM_Output( PWM_CH_ALL, 0 );
}

void HAL_TIM_PWM_Load( uint8_t motor_lr, int32_t pwm )
{
	__motor_t* motor = &Motor;
	
	if( motor_lr == MOTOR_L ) {
		if( motor->Status_bit.l_motor_front == true ) {
			if( pwm > 0 ) {
				HAL_TIM_PWM_Output( PWM_CH_ML_1, pwm );
				HAL_TIM_PWM_Output( PWM_CH_ML_2, 0 );
			}
			else if( pwm < 0 ) {
				HAL_TIM_PWM_Output( PWM_CH_ML_1, 0 );
				HAL_TIM_PWM_Output( PWM_CH_ML_2, abs(pwm) );
			}
			else {
				HAL_TIM_PWM_Output( PWM_CH_ML_1, 0 );
				HAL_TIM_PWM_Output( PWM_CH_ML_2, 0 );
			}
		}
		else {
			if( pwm > 0 ) {
				HAL_TIM_PWM_Output( PWM_CH_ML_1, 0 );
				HAL_TIM_PWM_Output( PWM_CH_ML_2, pwm );
			}
			else if( pwm < 0 ) {
				HAL_TIM_PWM_Output( PWM_CH_ML_1, abs(pwm) );
				HAL_TIM_PWM_Output( PWM_CH_ML_2, 0 );
			}
			else {
				HAL_TIM_PWM_Output( PWM_CH_ML_1, 0 );
				HAL_TIM_PWM_Output( PWM_CH_ML_2, 0 );
			}
		}
		return;
	}
	
	if( motor->Status_bit.r_motor_front == true ) {
		if( pwm > 0 ) {
			HAL_TIM_PWM_Output( PWM_CH_MR_1, pwm );
			HAL_TIM_PWM_Output( PWM_CH_MR_2, 0 );
		}
		else if( pwm < 0 ) {
			HAL_TIM_PWM_Output( PWM_CH_MR_1, 0 );
			HAL_TIM_PWM_Output( PWM_CH_MR_2, abs(pwm) );
		}
		else {
			HAL_TIM_PWM_Output( PWM_CH_MR_1, 0 );
			HAL_TIM_PWM_Output( PWM_CH_MR_2, 0 );
		}
	}
	else {
		if( pwm > 0 ) {
			HAL_TIM_PWM_Output( PWM_CH_MR_1, 0 );
			HAL_TIM_PWM_Output( PWM_CH_MR_2, pwm );
		}
		else if( pwm < 0 ) {
			HAL_TIM_PWM_Output( PWM_CH_MR_1, abs(pwm) );
			HAL_TIM_PWM_Output( PWM_CH_MR_2, 0 );
		}
		else {
			HAL_TIM_PWM_Output( PWM_CH_MR_1, 0 );
			HAL_TIM_PWM_Output( PWM_CH_MR_2, 0 );
		}
	}
}

void HAL_TIM_PWM_Out_En( uint8_t channel, bool enable )
{
	GPIO_PinState gpio_pinstate = ((enable == true) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET));
	
	switch( channel ) {
		case PWM_CH_ML_1:
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, gpio_pinstate);	//motor L1
			break;
		case PWM_CH_ML_2:
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, gpio_pinstate);	//motor L2
			break;
		case PWM_CH_MR_1:
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, gpio_pinstate);	//motor R1
			break;
		case PWM_CH_MR_2:
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, gpio_pinstate);	//motor R2
			break;
		default:
			break;
	}
}

/* USER CODE END 1 */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
