/* 
 * File:   SysMgmt.h
 * Author: cydai
 *
 * Created on oct 18, 2015, 11:07 AM
 */

#ifndef SYSMGMT_H
#define	SYSMGMT_H

#ifdef	__cplusplus
extern "C" {
#endif

typedef enum {
	SYS_EXCEP_NONE = 0,
	SYS_EXCEP_MOTOR_CONN,
	SYS_EXCEP_OVER_CURRENT,
	SYS_EXCEP_BATT_CAPACITY,
	SYS_EXCEP_HANDLE_CENTER,
	SYS_EXCEP_AVG_CURRENT,
	
	SYS_EXCEP_COMM,
	SYS_EXCEP_HANDLE_CONN,
	SYS_EXCEP_BREAK,
} __excep_type_t;

typedef enum {
	ERROR_NONE = 0,
	ERROR_MOTOR_CONN	= 0x00000001,
	ERROR_OVER_CURRENT	= 0x00000002,
	ERROR_BATT_CAPACITY	= 0x00000004,
	ERROR_HANDLE_CENTER	= 0x00000008,
	ERROR_COMM			= 0x00000010,
	ERROR_HANDLE_CONN	= 0x00000020,
	
	ERROR_L_BREAK		= 0x00000040,
	ERROR_R_BREAK		= 0x00000080,

} __error_code_t;

typedef struct {
	uint16_t  ChkStep;
	uint16_t: 16;
	uint32_t  ErrCode;
} __sys_excep_t;

typedef struct {
    uint32_t power_on:          		1;
	uint32_t enter_spd_calib_eng:		1;
	uint32_t enter_handle_eng:			1;
	uint32_t speaker_on:				1;
	uint32_t spd_calib_start:			1;
	uint32_t motor_conn_chked:			1;
	uint32_t handle_zero:				1;
	uint32_t para_got:         			1;
	
	uint32_t sys_init_done:				1;
} __sysstatus_bits_t;

/* System Management socket */
typedef struct System_Management_Socket {
	__sysstatus_bits_t Status; 
    __sys_excep_t 	sysExcep;
	uint16_t    	PowerCapacity;		//xx%
    uint16_t		CommTick_1ms;
	uint8_t     	CommTimeout;
	uint8_t: 		8;
	uint16_t    	CurrentOverCnt;
}sysmgmt_t;

extern __no_init sysmgmt_t SysMgmt;

void SysMgmt_Init( void );
bool SysMgmt_PowerChk( void );
void SysMgmt_DevicesInit( void );
void SysMgmt_ShutdownCtrl( void );
void SysMgmt_Power15V( bool enable );
void SysMgmt_ExceptionChk( sysmgmt_t* mgmt, uint8_t item );

void SysMgmt_ErrorSet( sysmgmt_t* mgmt, uint32_t err );
void SysMgmt_ErrorClr( sysmgmt_t* mgmt, uint32_t err );
bool SysMgmt_ErrorChk( sysmgmt_t* mgmt, uint32_t err );
void SysMgmt_ExpBuzzer( sysmgmt_t* mgmt );
void Sys_Reset( void );

#ifdef	__cplusplus
}
#endif

#endif	/* SYSMGMT_H */

