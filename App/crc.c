#include "include.h"

unsigned short Crc16(unsigned char *byte, int len)
{
    unsigned short crc = 0xffff;
    while (len--)
        crc = UpdateCrc16(crc, *byte++);
    return crc;
}

unsigned short UpdateCrc16(unsigned short crc, unsigned char byte)
{
    int i;

    crc ^= byte;
    unsigned short tmp = 0;

    for (i = 0; i < 8; i++)
        if (crc & 0x0001)
        {
            tmp = crc >> 1;
            tmp &= 0x7fff;
            crc = tmp ^ 0xA001;
        }
        else
        {
            tmp = crc >> 1;
            crc = tmp & 0x7fff;
        }

    return crc;
}