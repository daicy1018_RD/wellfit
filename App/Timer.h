#ifndef TIMER_H
#define	TIMER_H

#ifdef	__cplusplus
extern "C" {
#endif

#define TICK_1_ms                   1
#define TICK_5_ms                   5
#define TICK_10_ms                  10
#define TICK_20_ms                  20
#define TICK_50_ms                  50
#define TICK_100_ms                 100
#define TICK_500_ms                 500
#define TICK_1000_ms                1000

typedef struct {
	uint32_t Timer_1ms_Trig :      	1;
	uint32_t Timer_5ms_Trig :      	1;
	uint32_t Timer_10ms_Trig :      1;
	uint32_t Timer_20ms_Trig :      1;
    uint32_t Timer_50ms_Trig :      1;
    uint32_t Timer_100ms_Trig :     1;
    uint32_t Timer_500ms_Trig :     1;
	uint32_t Timer_1s_Trig :        1;
} TimerFlag_t;

extern TimerFlag_t TimerFlag;

void SysTimerHandler( void );

#ifdef	__cplusplus
}
#endif

#endif	/* TIMER_H */

